<?php

/* themes/fortytwo/templates/navigation/breadcrumb.html.twig */
class __TwigTemplate_c1c5de47cd2acedbc412e0375d88431ff177f678d41942b145306b257d74e533 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 13, "for" => 17);
        $filters = array("t" => 15);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if', 'for'),
                array('t'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 12
        echo "
";
        // line 13
        if ((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null)) {
            // line 14
            echo "  <nav role=\"navigation\" aria-labelledby=\"system-breadcrumb\">
    <h2 class=\"visually-hidden\">";
            // line 15
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Breadcrumb")));
            echo "</h2>
    <ol>
      ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 18
                echo "
        <li";
                // line 19
                if ($this->getAttribute($context["item"], "icon", array())) {
                    echo " data-icon=\"";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "icon", array()), "html", null, true));
                    echo "\"";
                }
                echo ">
          ";
                // line 20
                if ($this->getAttribute($context["item"], "url", array())) {
                    // line 21
                    echo "            <a href=\"";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true));
                    echo "\">";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "text", array()), "html", null, true));
                    echo "</a>
          ";
                } else {
                    // line 23
                    echo "            ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "text", array()), "html", null, true));
                    echo "
          ";
                }
                // line 25
                echo "        </li>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "    </ol>
  </nav>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/fortytwo/templates/navigation/breadcrumb.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 27,  87 => 25,  81 => 23,  73 => 21,  71 => 20,  63 => 19,  60 => 18,  56 => 17,  51 => 15,  48 => 14,  46 => 13,  43 => 12,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation for a breadcrumb trail.*/
/*  **/
/*  * Available variables:*/
/*  * - breadcrumb: Breadcrumb trail items.*/
/*  **/
/*  * @ingroup themeable*/
/*  *//* */
/* #}*/
/* */
/* {% if breadcrumb %}*/
/*   <nav role="navigation" aria-labelledby="system-breadcrumb">*/
/*     <h2 class="visually-hidden">{{ 'Breadcrumb'|t }}</h2>*/
/*     <ol>*/
/*       {% for item in breadcrumb %}*/
/* */
/*         <li{% if item.icon %} data-icon="{{ item.icon }}"{% endif %}>*/
/*           {% if item.url %}*/
/*             <a href="{{ item.url }}">{{ item.text }}</a>*/
/*           {% else %}*/
/*             {{ item.text }}*/
/*           {% endif %}*/
/*         </li>*/
/*       {% endfor %}*/
/*     </ol>*/
/*   </nav>*/
/* {% endif %}*/
/* */
