<?php

/* themes/fortytwo/templates/layout/page--user.html.twig */
class __TwigTemplate_1af9a9eb04d37dd567820838a3b36ba3717b6801cea677f88013628d4e122da2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 49);
        $filters = array("raw" => 108);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array('raw'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 48
        echo "
";
        // line 49
        if ((isset($context["ft_layout_login"]) ? $context["ft_layout_login"] : null)) {
            // line 50
            echo "  <div class=\"login-bg\"></div>
  <div class=\"login-overlay\"></div>
";
        }
        // line 53
        echo "
<header>
  ";
        // line 55
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
</header>

";
        // line 58
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array())) {
            // line 59
            echo "  <div class=\"navigation\">
    ";
            // line 60
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()), "html", null, true));
            echo "
  </div>
";
        }
        // line 63
        echo "
<div class=\"wrapper\">
  ";
        // line 65
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "breadcrumb", array()), "html", null, true));
        echo "

  <section class=\"content column\">
    ";
        // line 68
        if ((isset($context["messages"]) ? $context["messages"] : null)) {
            // line 69
            echo "      <div class=\"messages\">
        ";
            // line 70
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["messages"]) ? $context["messages"] : null), "html", null, true));
            echo "
      </div>
    ";
        }
        // line 73
        echo "    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array()), "html", null, true));
        echo "

    ";
        // line 75
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array())) {
            // line 76
            echo "      <div class=\"highlighted\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
            echo "</div>
    ";
        }
        // line 78
        echo "
    <a id=\"main-content\" tabindex=\"-1\"></a>

    ";
        // line 81
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
  </section>

  ";
        // line 84
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())) {
            // line 85
            echo "    <aside class=\"column sidebar first\">
      ";
            // line 86
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()), "html", null, true));
            echo "
    </aside>  <!-- End first aside. -->
  ";
        }
        // line 89
        echo "
  ";
        // line 90
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array())) {
            // line 91
            echo "    <aside class=\"column sidebar second\">
      ";
            // line 92
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array()), "html", null, true));
            echo "
    </aside> <!-- End second aside. -->
  ";
        }
        // line 95
        echo "
</div>

";
        // line 98
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()) || (isset($context["theme_credit"]) ? $context["theme_credit"] : null))) {
            // line 99
            echo "  <footer>
    ";
            // line 100
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
            echo "
    ";
            // line 101
            if ((isset($context["theme_credit"]) ? $context["theme_credit"] : null)) {
                // line 102
                echo "      <div class = \"theme-credit\">";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["theme_credit"]) ? $context["theme_credit"] : null), "html", null, true));
                echo "</div>
    ";
            }
            // line 104
            echo "  </footer>
";
        }
        // line 106
        echo "
";
        // line 107
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "ft_layout_login", array())) {
            // line 108
            echo "  <div class=\"ft-credits\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "ft_credit", array())));
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/fortytwo/templates/layout/page--user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 108,  174 => 107,  171 => 106,  167 => 104,  161 => 102,  159 => 101,  155 => 100,  152 => 99,  150 => 98,  145 => 95,  139 => 92,  136 => 91,  134 => 90,  131 => 89,  125 => 86,  122 => 85,  120 => 84,  114 => 81,  109 => 78,  103 => 76,  101 => 75,  95 => 73,  89 => 70,  86 => 69,  84 => 68,  78 => 65,  74 => 63,  68 => 60,  65 => 59,  63 => 58,  57 => 55,  53 => 53,  48 => 50,  46 => 49,  43 => 48,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to display a single page.*/
/*  **/
/*  * The doctype, html, head and body tags are not in this template. Instead they*/
/*  * can be found in the html.html.twig template in this directory.*/
/*  **/
/*  * Available variables:*/
/*  **/
/*  * General utility variables:*/
/*  * - base_path: The base URL path of the Drupal installation. Will usually be*/
/*  *   "/" unless you have installed Drupal in a sub-directory.*/
/*  * - is_front: A flag indicating if the current page is the front page.*/
/*  * - logged_in: A flag indicating if the user is registered and signed in.*/
/*  * - is_admin: A flag indicating if the user has permission to access*/
/*  *   administration pages.*/
/*  **/
/*  * Site identity:*/
/*  * - front_page: The URL of the front page. Use this instead of base_path when*/
/*  *   linking to the front page. This includes the language domain or prefix.*/
/*  **/
/*  * Page content (in order of occurrence in the default page.html.twig):*/
/*  * - messages: Status and error messages. Should be displayed prominently.*/
/*  * - node: Fully loaded node, if there is an automatically-loaded node*/
/*  *   associated with the page and the node ID is the second argument in the*/
/*  *   page's path (e.g. node/12345 and node/12345/revisions, but not*/
/*  *   comment/reply/12345).*/
/*  **/
/*  * Regions:*/
/*  * - page.header: Items for the header region.*/
/*  * - page.primary_menu: Items for the primary menu region.*/
/*  * - page.secondary_menu: Items for the secondary menu region.*/
/*  * - page.highlighted: Items for the highlighted content region.*/
/*  * - page.help: Dynamic help text, mostly for admin pages.*/
/*  * - page.content: The main content of the current page.*/
/*  * - page.sidebar_first: Items for the first sidebar.*/
/*  * - page.sidebar_second: Items for the second sidebar.*/
/*  * - page.footer: Items for the footer region.*/
/*  * - page.breadcrumb: Items for the breadcrumb region.*/
/*  **/
/*  * @see template_preprocess_page()*/
/*  * @see html.html.twig*/
/*  **/
/*  * @ingroup themeable*/
/*  *//* */
/* #}*/
/* */
/* {% if ft_layout_login %}*/
/*   <div class="login-bg"></div>*/
/*   <div class="login-overlay"></div>*/
/* {% endif %}*/
/* */
/* <header>*/
/*   {{ page.header }}*/
/* </header>*/
/* */
/* {% if page.navigation %}*/
/*   <div class="navigation">*/
/*     {{ page.navigation }}*/
/*   </div>*/
/* {% endif %}*/
/* */
/* <div class="wrapper">*/
/*   {{ page.breadcrumb }}*/
/* */
/*   <section class="content column">*/
/*     {% if messages %}*/
/*       <div class="messages">*/
/*         {{ messages }}*/
/*       </div>*/
/*     {% endif %}*/
/*     {{ page.help }}*/
/* */
/*     {% if page.highlighted %}*/
/*       <div class="highlighted">{{ page.highlighted }}</div>*/
/*     {% endif %}*/
/* */
/*     <a id="main-content" tabindex="-1"></a>*/
/* */
/*     {{ page.content }}*/
/*   </section>*/
/* */
/*   {% if page.sidebar_first %}*/
/*     <aside class="column sidebar first">*/
/*       {{ page.sidebar_first }}*/
/*     </aside>  <!-- End first aside. -->*/
/*   {% endif %}*/
/* */
/*   {% if page.sidebar_second %}*/
/*     <aside class="column sidebar second">*/
/*       {{ page.sidebar_second }}*/
/*     </aside> <!-- End second aside. -->*/
/*   {% endif %}*/
/* */
/* </div>*/
/* */
/* {% if page.footer or theme_credit %}*/
/*   <footer>*/
/*     {{ page.footer }}*/
/*     {% if theme_credit %}*/
/*       <div class = "theme-credit">{{ theme_credit }}</div>*/
/*     {% endif %}*/
/*   </footer>*/
/* {% endif %}*/
/* */
/* {% if page.ft_layout_login %}*/
/*   <div class="ft-credits">{{ page.ft_credit | raw }}</div>*/
/* {% endif %}*/
/* */
